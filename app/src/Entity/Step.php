<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
 * collectionOperations={
 *      "post"
 * })
 * @ORM\Entity(repositoryClass="App\Repository\StepRepository")
 */
class Step
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $place;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceAccommodation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceTransport;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Travel", inversedBy="steps")
     * @ORM\JoinColumn(nullable=false)
     */
    private $travel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Transport")
     * @ORM\JoinColumn(nullable=false)
     */
    private $transport;

    public function __construct($place, $address, $priceAccomodation, $priceTransport, $travel, $transport) {
      $this->place = $place;
      $this->priceAccomadation = $priceAccomodation;
      $this->address = $address;
      $this->priceAccomadation = $priceAccomodation;
      $this->priceTransport = $priceTransport;
      $this->travel = $travel;
      $this->transport = $transport;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPriceAccommodation(): ?int
    {
        return $this->priceAccommodation;
    }

    public function setPriceAccommodation(?int $priceAccommodation): self
    {
        $this->priceAccommodation = $priceAccommodation;

        return $this;
    }

    public function getPriceTransport(): ?int
    {
        return $this->priceTransport;
    }

    public function setPriceTransport(?int $priceTransport): self
    {
        $this->priceTransport = $priceTransport;

        return $this;
    }

    public function getTravel(): ?Travel
    {
        return $this->travel;
    }

    public function setTravel(?Travel $travel): self
    {
        $this->travel = $travel;

        return $this;
    }

    public function getTransport(): ?Transport
    {
        return $this->transport;
    }

    public function setTransport(?Transport $transport): self
    {
        $this->transport = $transport;

        return $this;
    }
}
