<?php

namespace App\Controller;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Service\LoggerFactory;

class GetTravels
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function __invoke(LoggerFactory $loggerFactory): array
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $logger = $loggerFactory->getLogger('travel');
        $logger->info("getTravels", ["user" => $user->getUsername(), "travels" => $user->getTravels()->count()]);
        return [$user->getTravels()];
    }
}
