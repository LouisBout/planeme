<?php

namespace App\Controller;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Service\LoggerFactory;

class GetCurrentUser
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function __invoke(LoggerFactory $loggerFactory): array
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $logger = $loggerFactory->getLogger('user');
        $logger->info("getCurrentUser", ["user" => $user->getUsername()]);
        return [$user];
    }
}