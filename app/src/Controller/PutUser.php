<?php

namespace App\Controller;

use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

class PutUser
{

    public function __construct(ManagerRegistry $managerRegistry, ResourceMetadataFactoryInterface $resourceMetadataFactory)
    {
      $this->managerRegistry = $managerRegistry;
      $this->resourceMetadataFactory = $resourceMetadataFactory;
    }

    public function __invoke(Request $request, UserInterface $user): User
    {
        $uploadedFile = $request->get()->get('avatar');

        $user->setImageFile($uploadedFile);

        $em = $this->managerRegistry->getManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }
}
