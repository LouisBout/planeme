<?php

namespace App\Service;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MailerManager {

  private $params;
  private $mailer;

  public function __construct (ParameterBagInterface $params, \Twig\Environment $templating, \Swift_Mailer $mailer) {
    $this->params = $params;
    $this->mailer = $mailer;
    $this->templating = $templating;
  }

  public function sendMailAfterRegistration ($username, $email) {
    $message = (new \Swift_Message('Bienvenue sur PlaneMe !'))
    ->setFrom('pierre@planeme.fr')
    ->setTo($email)
    ->setBody(
        $this->templating->render(
            'email/registration.html.twig',
            ['username' => $username]
          ),
          'text/html'
      );

    $this->mailer->send($message);
  }
}
