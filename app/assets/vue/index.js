// Default imports
import Vue from 'vue'
import router from '@/router'
import store from '@/store'
import App from './App'

// Additionnal imports
import 'assets/scss/styles.scss'

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})