import axios from 'axios'

export default axios.create({
  baseURL: `http://${process.env.APP_PUBLIC_URL}/api`,
  headers: {
    Accept: 'application/json'
  }
})
