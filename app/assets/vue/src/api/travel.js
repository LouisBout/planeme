import Api from './'

export default {
  createTravel: ({ formTravel}) => Api.post('/travels', formTravel),
}
