import Api from './'

export default {
  getCurrentUser: () => Api.get('/users/current'),
  updateUser: (id, params) => Api.put(`/users/${id}`, params)
}
