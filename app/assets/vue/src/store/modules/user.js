import user from '@/api/user'

const state = {
  user: {},
  error: null
}

// getters
const getters = {
  user: () => state.user
}

// actions
const actions = {
  getCurrentUser: async ({ commit }) => {
    try{
      const response = await user.getCurrentUser()
      commit('getCurrentUserMutation', response)
    }catch(error){
      commit('fetchErrorMutation', error)
    }
  }
}

// mutations
const mutations = {
  getCurrentUserMutation (state, { data }) {
    state.user = data[0]
    state.error = null
  },
  fetchErrorMutation: (state, error) => {
    state.error = error
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}