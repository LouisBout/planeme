import travel from '@/api/travel'

const state = {
  travel: {},
  isFormVisible: false,
  error: null
}

// getters
const getters = {
  travel: () => state.travel,
  isFormVisible: () => state.isFormVisible
}

// actions
const actions = {
  createTravel: async ({ commit }, { formTravel }) => {
    try{
      const response = await travel.createTravel({formTravel})
      commit('createTravelMutation', response)
    }catch(error){
      commit('errorMutation', error)
    }
  },
  toggleForm: ({ commit }) => {
    commit('toggleFormMutation')
  }
}

// mutations
const mutations = {
  createTravelMutation (state, { data }) {
    state.travel = data
    state.error = null
  },
  errorMutation: (state, error) => {
    state.error = error
  },
  toggleFormMutation (state) {
    state.isFormVisible = !state.isFormVisible
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
