<?php

namespace app\tests;

use App\Entity\Travel;
use App\Entity\Step;
use PHPUnit\Framework\TestCase;

class stepTest extends TestCase
{

  public function testAddStep()
  {
    $travel = new Travel();

    for($i=0; $i<3; $i++) {
      $step = new Step(1,1, 'Nantes', '8 rue du Chateau', 200, 150);
      $travel->addStep($step);
    }

    $steps = $travel->getSteps();

    $this->assertCount(3, $steps);
  }

}
