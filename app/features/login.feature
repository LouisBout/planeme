Feature:
    In order to prove that the login form is working correcty
    As a user
    I want to sign in properly

    Scenario: The user goes to the login page
        Given I am on "/login"
        When I fill in "username" with "louis"
        Then I fill in "password" with "louisbout"
        And I press "Sign in"
        Then I should see "Hello"
