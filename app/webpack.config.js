const Encore = require('@symfony/webpack-encore');
const path = require('path');
const Dotenv = require('dotenv-webpack');
require('dotenv').config();

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath(`http://${process.env.APP_URL}:${process.env.WEBPACK_PORT}/build`)
    // only needed for CDN's or sub-directory deploy
    .setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('app', './assets/vue/index.js')
    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    //.enableSingleRuntimeChunk()
    .disableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
    .enableVueLoader()

    // enables Sass/SCSS support
    .enableSassLoader()

    .addPlugin(new Dotenv({systemvars: true}))

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()
    .configureBabel(function(babelConfig) {
      babelConfig.presets = ['@babel/preset-env']
      babelConfig.plugins = ['@babel/plugin-transform-regenerator', '@babel/transform-runtime', '@babel/plugin-transform-async-to-generator']
    })
    .configureWatchOptions(watchOptions => {
      watchOptions.poll = 250; // check for changes every 250 ms
    });
;

const config = Encore.getWebpackConfig();

config.devServer = {
  host: '0.0.0.0',
  public: `${process.env.APP_URL}:${process.env.WEBPACK_PORT}`,
  allowedHosts: [`${process.env.APP_URL}`],
  disableHostCheck: true
};

config.resolve.alias["@"] = path.resolve(__dirname, 'assets/vue/src');
config.resolve.alias["assets"] = path.resolve(__dirname, 'assets');

module.exports = config;
